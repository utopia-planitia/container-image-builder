# Container Image Builder

A service to build docker images.

Uses docker-in-docker internally.

## Usage

Run `kubectl -n container-image-builder exec -it deployment/devtools -- sh` to access the builder.

## Attributions

Icon made by [Solid Professions Avatars Vectors](https://www.svgrepo.com/vectors/solid-professions-avatars/) from [svgrepo.com](https://www.svgrepo.com/)
