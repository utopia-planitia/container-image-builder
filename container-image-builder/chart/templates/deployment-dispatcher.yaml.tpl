apiVersion: apps/v1
kind: Deployment
metadata:
  name: dispatcher
spec:
  selector:
    matchLabels:
      app: dispatcher
  replicas: 1
  template:
    metadata:
      labels:
        app: dispatcher
    spec:
      containers:
        - name: dispatcher
          image: utopiaplanitia/docker-image-builder-dispatcher:1.6.0@sha256:928cbab1b76f6ab839415e8eefec0342dbfe2dcc176e21a8e39c436d99d303be
          args:
            - --workers={{- range $val := until (int .Values.replicas) }}{{ if $val }},{{ end }}http://builder-{{ $val }}.builder.container-image-builder.svc.cluster.local:2375{{- end }}
            - --memory=4294967296
            - --cpu=2000000
          ports:
            - containerPort: 2375
              name: docker
          livenessProbe:
            tcpSocket:
              port: 2375
          readinessProbe:
            httpGet:
              path: /healthz
              port: 2375
          resources:
            requests:
              memory: 30Mi
              cpu: 30m
            limits:
              memory: 30Mi
              cpu: 30m
